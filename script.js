// Теоретичні питання
// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Т.к. есть другие способы ввести данные, типа распознавания речи и ввода с клавиатуры не достаточно. есть отдельное событие input что бы учесть все изменения.

// Завдання
// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

// Вар.1
const btns = document.querySelectorAll(".btn");

document.addEventListener("keydown", (e) => {
  btns.forEach((i) => {
    const keyUpp = e.key.toUpperCase();
    const btnsUpp = i.textContent.toUpperCase();
    const boolean1 = keyUpp === btnsUpp;
    highlight(i, boolean1);
  });
});

function highlight(elem, boolean) {
  elem.style.backgroundColor = boolean ? "blue" : "";
}

//--------------------------------------------------------

// Вар.2

// const color = "highligted";
// const wrapper = document.querySelector(".btn-wrapper");

// document.body.addEventListener("keydown", (e) => {
//   const btn = wrapper.querySelector(`[data-key=${e.key}]`);
//   if (btn) {
//     const prev = wrapper.querySelector(".highligted");
//     if (prev) {
//       prev.classList.remove(color);
//     }
//     btn.classList.add(color);
//   }
// });

//------------------------------------------------------

//Вар 3.

// function colorButtons() {
//   const btns = document.querySelectorAll(".btn");
//   document.addEventListener("keydown", function (e) {
//     let pressKey = e.key.toLocaleUpperCase();
//     btns.forEach((item) => {
//       let itemUt = item.textContent.toLocaleUpperCase();
//       if (pressKey === itemUt) {
//         item.style.backgroundColor = "blue";
//       } else {
//         item.style.backgroundColor = "";
//       }
//     });
//   });
// }

// colorButtons();
